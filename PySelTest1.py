#!/usr/bin/python3
#file test_1.py
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import sys, getopt
import time

#print ('Number of arguments:', len(sys.argv), 'arguments.')
#print ('Argument List:', str(sys.argv))  # first one is file name
					 
					 
driver = webdriver.Remote(
   command_executor="http://localhost:4444/wd/hub",
   desired_capabilities={
            "browserName": "chrome",
            "name":"Add New Provider",
            "build" : "Build 2031"
        })

def gloop1():
    # create a new Firefox session
    #driver = webdriver.Firefox()
    driver.implicitly_wait(30)
    driver.maximize_window()

    # navigate to the application home page
    driver.get("http://www.google.com")

    # get the search textbox
    search_field = driver.find_element_by_id("lst-ib")
    search_field.clear()

    # enter search keyword and submit
    search_field.send_keys("Selenium WebDriver Interview questions")
    search_field.submit()

    # get the list of elements which are displayed after the search
    # currently on result page using find_elements_by_class_name  method
    lists= driver.find_elements_by_class_name("_Rm")

    # get the number of elements found
    #print (“Found “ + str(len(lists)) + “searches:”)

    # iterate through each element and print the text that is
    # name of the search

    #i=0
    #for listitem in lists:
    #Print(listitem)
    #i=i+1
    #if(i>10):
    #    break
  
def ploop1():
    driver.get("http://www.python.org")
    assert "Python" in driver.title
    elem = driver.find_element_by_name("q")
    elem.send_keys("documentation")
    elem.send_keys(Keys.RETURN)
    assert "No results found." not in driver.page_source


try:
    driver.implicitly_wait(30)
    driver.maximize_window() # Note: driver.maximize_window does not work on Linux selenium version v2, instead set window size and window position like driver.set_window_position(0,0) and driver.set_window_size(1920,1080)
    for x in range(1,12):
        gloop1()
        time.sleep(1)
        ploop1()
        time.sleep(1)
    #driver.implicitly_wait(30)
    #driver.get("https://www.therabill.com")
    #driver.implicitly_wait(30)

finally:
    driver.quit()
